import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
// import { ToastController } from '@ionic/angular';
// import { LoadingController } from '@ionic/angular';
 

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  // loaderToShow: any;
  // isLoading : boolean = false;

  constructor(private router: Router
    //,public toastController: ToastController, public loadingController: LoadingController
    ) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    //localStorage.setItem('access_token','blanktoken');
    //let token = "C9Y7UK/5p7vX3crKnPXGLBjSbBvkNMK6mm+K8EnRYXaI1V1Mr6yxATNRIasAasZb8CXS/L1BOJbA8HJcX0+DeNzZVrZ3FqK94QvBMktwNWTFkknrVIxq7DaBUN0tbK8Vigp59CkFiLcuzMyzLiVBw7XxMtMp5VFbNwFOA+gFmBCMsZq688c8OLsenvAHRbTa76VCkkWLtvAXHdbov9Ju+lH+eZ/4v1JNI8oeEDsFzFM8aRemc6FM9wx4EOZOW2r5";
    localStorage.setItem('access_tokenCopy',localStorage.getItem('access_token'));
    let token = localStorage.getItem('access_tokenCopy');
    console.info('Tokenku= '+token);

    if (token) {
        request = request.clone({ headers: request.headers.set('Authorization', token) });
    }

    if (!request.headers.has('Content-Type')) {
      request = request.clone({
        setHeaders: {
          'content-type': 'application/json'
        }
      });
    }

    request = request.clone({
      headers: request.headers.set('Accept', 'application/json')
    });
    
    return next.handle(request).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          //console.log('event--->>> ', event);
        }
      //  this.presentToast("Finish ") ;         
        return event;
      }),
      catchError((error: HttpErrorResponse) => {
        if (error.status === 401) {
          if (error.error.success === false) {
            this.presentToast('Wrong username or password');
          } else {
           //this.router.navigate(['auth']);
           console.log('You must login first !');
           window.location.href = 'http://10.126.105.31:4040/Login/';
          }
        }
        //if (this.isLoading )  this.hideLoader();
        console.error('HttpErrorResponse --->>> ', event);
        return throwError(error);
      }));
  }

   async presentToast(msg) {
  //   const toast = await this.toastController.create({
  //     message: msg,
  //     duration: 2000,
  //     position: 'top'
  //   });
  //   toast.present();
  console.log(msg);
   }
}

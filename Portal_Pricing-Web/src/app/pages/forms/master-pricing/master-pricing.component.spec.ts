import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterPricingComponent } from './master-pricing.component';

describe('MasterPricingComponent', () => {
  let component: MasterPricingComponent;
  let fixture: ComponentFixture<MasterPricingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterPricingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterPricingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

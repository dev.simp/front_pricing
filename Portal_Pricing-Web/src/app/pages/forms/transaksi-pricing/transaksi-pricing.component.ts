import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { Observable } from "rxjs/Observable";
import { Subject, ReplaySubject } from 'rxjs';
import { NbToastrService } from '@nebular/theme';
import { SERVER_URL } from '../../../../environments/environment';
import { MatSelect } from '@angular/material/select';
import { take, takeUntil } from 'rxjs/operators';
import {FormControl, Validators} from '@angular/forms';

interface Material {
  MaterialCode: Int32Array;
  MaterialDescription: string;
}

@Component({
  selector: 'ngx-transaksi-pricing',
  templateUrl: './transaksi-pricing.component.html',
  styleUrls: ['./transaksi-pricing.component.scss']
})
export class TransaksiPricingComponent implements OnDestroy, OnInit {
  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  DataProduct :  Object;
  DTA : any = {};
  isEditMode : boolean = false;
  isInsertMode : boolean = false;
  isListMode : boolean = true;

  DataPrice: any[] = [];
  PRICETYPE : any = {};
  materialData: any[] = [];
  prcTypeData: any[] = [];

  
  /** control for the selected bank */
  public matCtrl: FormControl = new FormControl();
  
  /** control for the MatSelect filter keyword */
  public matFilterCtrl: FormControl = new FormControl();
  
  /** list of banks filtered by search keyword */
  public filteredMats: ReplaySubject<Material[]> = new ReplaySubject<Material[]>(1);

  /** list of banks filtered by search keyword */
  public filteredMatsFinal: ReplaySubject<Material[]> = new ReplaySubject<Material[]>(1);
  
  @ViewChild('singleSelect', { static: true }) singleSelect: MatSelect;
  
  /** Subject that emits when the component has been destroyed. */
  protected _onDestroy = new Subject<void>();

  CURRENCY: any = [
    {CURRENCY: 'IDR', CURRENCY_VALUE: 'IDR'},
    {CURRENCY: 'USD', CURRENCY_VALUE: 'USD'},
  ];
  private index: number = 0;
  constructor( private toastrService: NbToastrService, private datePipe: DatePipe, private http: HttpClient) {
    this.http.get(SERVER_URL+'api/Price/GetMaterial/').subscribe(
      data => {           
        this.materialData = JSON.parse(data.toString());
    }, 
      error=> {
        console.error('Failed = '+JSON.stringify(error));
        this.showToast('top-right', 'warning','Error, Please Check your data');
    });  
    
   }

   getMaterialCd(id){
    this.http.get(SERVER_URL+'api/Price/GetPriceType/'+id).subscribe(
      data => {           
        this.prcTypeData = JSON.parse(data.toString());
    }, 
      error=> {
        console.error('Failed = '+JSON.stringify(error));
        this.showToast('top-right', 'warning','Error, Please Check your data');
    });  
   }

  getDatas() : Observable <any> {
    return this.http.get(SERVER_URL+'api/Price/GetPrices');
  }
  
  getDataMath() : Observable <any> {
    return this.http.get(SERVER_URL+'api/Price/GetMaterial/');
  }


  ngOnInit() {
    
    this.dtOptions = {
      destroy: true
    };
    this.getDatas().subscribe(data => {
      this.DataPrice = JSON.parse(data.toString());
      this.dtTrigger.next();
    }); 
    this.getDataMath().subscribe(data => {
      this.filteredMatsFinal = JSON.parse(data);
      //this.dtTrigger.next();
    }); 

    //load the initial bank list
    //this.filteredMatsFinal.next(this.materialData.slice());

    this.matFilterCtrl.valueChanges
    .pipe(takeUntil(this._onDestroy))
    .subscribe(() => {
      this.filterMats();
    });
  }

  protected filterMats() {
   
    if (!this.materialData) {
      return;
    }
    // get the search keyword
    let search = this.matFilterCtrl.value;
    if (!search) {
      this.filteredMats.next(this.materialData.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks

    this.filteredMats.next(
        this.materialData.filter(materialData => materialData.MaterialDescription.toLowerCase().indexOf(search) > -1)
     );
     console.info(JSON.stringify(this.filteredMats['_events'][0]));
     this.filteredMatsFinal = this.filteredMats['_events'][0];
  }

  protected setInitialValue() {
    this.filteredMats
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelect.compareWith = (a: Material, b: Material) => a && b && a.MaterialDescription === b.MaterialDescription;
      });
  }

  ngAfterViewInit() {
    this.setInitialValue();
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
    this.dtTrigger.unsubscribe();
  }

  isFormValid() {
    let formCompleted = this.DTA['MATERIAL'] != undefined && 
    this.DTA['PRICETYPE'] != undefined &&
    this.DTA['PRICE'] != undefined &&
    this.DTA['DATE'] != undefined &&
    this.DTA['CURRENCY'] != undefined ;
  return !formCompleted;
  }

  async DeleteConfirm(id,i) {
    this.http.delete(SERVER_URL+'api/Price/DeletePrice/'+id).subscribe(
      data => {   
        this.DataPrice.splice(i,1);
        this.isEditMode = false;
        this.isInsertMode = false;
        this.isListMode = true;
        this.ngOnInit();
        $('#example').DataTable().destroy();
        this.dtTrigger.next();
        this.showToast('top-right', 'success','Your Data Success Deleted');
    }, 
      error=> {
        console.error('Failed = '+JSON.stringify(error))
        this.isEditMode = false;
        this.isInsertMode = false;
        this.isListMode = true;
        this.showToast('top-right', 'warning','Error, Delete Failed Please Check your data');
    });
  }

  showToastErr(position, status, message) {
    this.index += 1;
    this.toastrService.show(
      status || 'Success',
      message,
      { position, status });
  }
  

  transformDate(date) {
    this.datePipe.transform(date, 'yyyy-MM-dd'); 
  }

  EditPrice(id){
    this.http.get(SERVER_URL+'api/Price/GetPrice/'+id).subscribe(
      data => {           
        this.DTA = data;
        this.DTA.ID = data['ID'];
        this.DTA.MATERIAL = data['MATERIAL'];
        this.DTA.PRICETYPE = data['PRICETYPE'];
        this.DTA.PRICE = data['PRICE'];
        this.DTA.DATE = this.datePipe.transform(data['DATE'], 'yyyy-MM-dd');
        this.DTA.CURRENCY = data['CURRENCY'];
        this.isInsertMode = false;
        this.isEditMode = true;
        this.isListMode = false;
        this.PRICETYPE = data['PRICETYPE'];
        this.getMaterialCd(data['MATERIAL']);

        console.error('Failed = '+JSON.stringify(this.DTA));
    }, 
      error=> {
        console.error('Failed = '+JSON.stringify(error))
    });    
  }
  
  CancelData() {
   this.isEditMode = false;
   this.isInsertMode = false;
   this.isListMode = true;
  }

  AddData() {
    this.DTA = {};
    this.isInsertMode = true;
    this.isEditMode = false;
    this.isListMode = false;
  }

  SaveData() {
    let PriceData = {
        'MATERIAL' : this.DTA.MATERIAL,
        'PRICE' : this.DTA.PRICE,
        'DATE' : this.DTA.DATE,
        'PRICETYPE' : this.DTA.PRICETYPE,
        'CURRENCY' :  this.DTA.CURRENCY		
    } 
       this.http.post(SERVER_URL+'api/Price/SavePrice',PriceData).subscribe(
        data => {    
          console.log(data);
          if(data){
            this.showToast('top-right', 'success','Your Data Success Saved');
          } else {
            this.showToast('top-right', 'warning','Error, Save Failed Please Check your data');
          }
          this.isEditMode = false;
          this.isInsertMode = false;
          this.isListMode = true;
          this.ngOnInit();
          this.DTA = [];
          $('#example').DataTable().destroy();
          this.dtTrigger.next(); 
        },         
          error=> {
            console.error('SERVER_URL = '+JSON.stringify(error))
            this.showToast('top-right', 'warning','Error, Please Check your data');
            this.isEditMode = false;
            this.isInsertMode = false;
            this.isListMode = true;
        });
  }

  showToast(position, status, message) {
    this.index += 1;
    this.toastrService.show(
      status || 'Success',
      message,
      { position, status });
  }
  
  
  UpdateData(id) {
    let PriceData = {
        'ID' : this.DTA.ID,
        'MATERIAL' : this.DTA.MATERIAL,
        'PRICE' : this.DTA.PRICE,
        'DATE' : this.DTA.DATE,
        'PRICETYPE' : this.DTA.PRICETYPE,
        'CURRENCY' :  this.DTA.CURRENCY		
      } 
        console.log(JSON.stringify(PriceData));
        this.http.post(SERVER_URL+'api/Price/UpdatePrice/'+id,PriceData).subscribe(
        data => { 
          console.log(data);
          if(data){
            this.showToast('top-right', 'success','Your Data Success Updated');
          } else {
            this.showToast('top-right', 'warning','Error, Update Failed Please Check your data');
          }
          this.isEditMode = false;
          this.isInsertMode = false;
          this.isListMode = true;
          this.DTA = [];
          this.ngOnInit();
          $('#example').DataTable().destroy();
          this.dtTrigger.next();
        },         
          error=> {
            console.error('SERVER_URL = '+JSON.stringify(error));
            this.showToast('top-right', 'warning','Error, Please Check your data');
            this.isEditMode = false;
            this.isInsertMode = false;
            this.isListMode = true;
        });
  }
}

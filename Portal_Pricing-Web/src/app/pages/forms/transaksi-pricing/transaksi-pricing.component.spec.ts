import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TransaksiPricingComponent } from './transaksi-pricing.component';

describe('TransaksiPricingComponent', () => {
  let component: TransaksiPricingComponent;
  let fixture: ComponentFixture<TransaksiPricingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransaksiPricingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransaksiPricingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

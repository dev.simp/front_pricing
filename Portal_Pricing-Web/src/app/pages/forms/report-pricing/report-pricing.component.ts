import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { Observable } from "rxjs/Observable";
import { Subject, ReplaySubject } from 'rxjs';
import { NbToastrService } from '@nebular/theme';
import { SERVER_URL } from '../../../../environments/environment';
import { MatSelect } from '@angular/material/select';
import { take, takeUntil } from 'rxjs/operators';
import {FormControl, Validators} from '@angular/forms';
import * as XLSX from 'xlsx'; 

interface Material {
  MaterialCode: Int32Array;
  MaterialDescription: string;
}

@Component({
  selector: 'ngx-report-pricing',
  templateUrl: './report-pricing.component.html',
  styleUrls: ['./report-pricing.component.scss']
})
export class ReportPricingComponent implements OnDestroy, OnInit  {
  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  DataProduct :  Object;
  DTA : any = {};
  fileName= 'ReportPricing.xlsx';  
  DataPrice: any[] = [];
  private index: number = 0;
  materialData: any[] = [];
  prcTypeData: any[] = [];
  ReportMode : boolean = false;

  /** control for the selected bank */
  public matCtrl: FormControl = new FormControl();
  
  /** control for the MatSelect filter keyword */
  public matFilterCtrl: FormControl = new FormControl();
    
  /** list of banks filtered by search keyword */
  public filteredMats: ReplaySubject<Material[]> = new ReplaySubject<Material[]>(1);
  
  /** list of banks filtered by search keyword */
  public filteredMatsFinal: ReplaySubject<Material[]> = new ReplaySubject<Material[]>(1);
    
  @ViewChild('singleSelect', { static: true }) singleSelect: MatSelect;
    
  /** Subject that emits when the component has been destroyed. */
  protected _onDestroy = new Subject<void>();
  
  constructor(private toastrService: NbToastrService, private datePipe: DatePipe, private http: HttpClient) { 

    this.http.get(SERVER_URL+'api/Price/GetMaterial/').subscribe(
      data => {           
        this.materialData = JSON.parse(data.toString());
        console.info(JSON.stringify(data));
    }, 
      error=> {
        console.error('Failed = '+JSON.stringify(error));
        this.showToast('top-right', 'warning','Error, Please Check your data');
    });   
     
  }

  getDataMath() : Observable <any> {
    return this.http.get(SERVER_URL+'api/Price/GetMaterial/');
  }

  getMaterialCd(id){
    this.http.get(SERVER_URL+'api/Price/GetPriceType/'+id).subscribe(
      data => {           
        this.prcTypeData = JSON.parse(data.toString());
        console.info(JSON.stringify(data));
    }, 
      error=> {
        console.error('Failed = '+JSON.stringify(error));
        this.showToast('top-right', 'warning','Error, Please Check your data');
    });  
   }



   
  showToast(position, status, message) {
    this.index += 1;
    this.toastrService.show(
      status || 'Success',
      message,
      { position, status });
  }
  

   getMaterialFilter(){
      this.ReportMode = true;
      this.dtOptions = {
        destroy: true
      };
      this.getDatas().subscribe(data => {
        this.DataPrice = JSON.parse(data);
        this.dtTrigger.next();
      });
     
   }

   getDatas() : Observable <any> {
    return  this.http.get(SERVER_URL+'api/Price/GetPriceTypeFilter?mtr=' + this.DTA.MATERIAL + '&prc=' + this.DTA.PRICETYPE + '&df=' + this.DTA.DATE_FROM + '&dt=' + this.DTA.DATE_TO);
   }

  clearData(){
    this.DataPrice = [];
    this.DTA.DATE_FROM = undefined;
    this.DTA.DATE_TO = undefined;
    this.DTA.PRICETYPE = '';
    this.DTA.MATERIAL = '';
    this.ReportMode = false;
    $('#example').DataTable().destroy();
    this.ngOnInit(); 
  }

  isFormValid() {
    let formCompleted = this.DTA['DATE_FROM'] != undefined && 
    this.DTA['DATE_TO'] != undefined;
  return !formCompleted;
  }

  exportexcel(): void 
    {
       /* table id is passed over here */   
       let element = document.getElementById('excel-table'); 
       const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);

       /* generate workbook and add the worksheet */
       const wb: XLSX.WorkBook = XLSX.utils.book_new();
       XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

       /* save to file */
       XLSX.writeFile(wb, this.fileName);
			
    }

 
  ngOnInit() {
    this.dtOptions = {
      destroy: true
    };
    this.getDatas().subscribe(data => {
      this.DataPrice = JSON.parse(data.toString());
      this.dtTrigger.next();
    }); 
    this.getDataMath().subscribe(data => {
      this.filteredMatsFinal = JSON.parse(data);
      //this.dtTrigger.next();
    }); 

    //load the initial bank list
    //this.filteredMatsFinal.next(this.materialData.slice());

    this.matFilterCtrl.valueChanges
    .pipe(takeUntil(this._onDestroy))
    .subscribe(() => {
      this.filterMats();
    });

  }

  protected filterMats() {
   
    if (!this.materialData) {
      return;
    }
    // get the search keyword
    let search = this.matFilterCtrl.value;
    if (!search) {
      this.filteredMats.next(this.materialData.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks

    this.filteredMats.next(
        this.materialData.filter(materialData => materialData.MaterialDescription.toLowerCase().indexOf(search) > -1)
     );
     console.info(JSON.stringify(this.filteredMats['_events'][0]));
     this.filteredMatsFinal = this.filteredMats['_events'][0];
  }

  protected setInitialValue() {
    this.filteredMats
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelect.compareWith = (a: Material, b: Material) => a && b && a.MaterialDescription === b.MaterialDescription;
      });
  }

  ngAfterViewInit() {
    this.setInitialValue();
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
    this.dtTrigger.unsubscribe();
  }
}



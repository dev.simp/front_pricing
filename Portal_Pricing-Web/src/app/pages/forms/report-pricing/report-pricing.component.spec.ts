import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReportPricingComponent } from './report-pricing.component';

describe('ReportPricingComponent', () => {
  let component: ReportPricingComponent;
  let fixture: ComponentFixture<ReportPricingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportPricingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportPricingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { Observable } from "rxjs/Observable";
import { Subject, ReplaySubject } from 'rxjs';
import { NbToastrService } from '@nebular/theme';
import { SERVER_URL } from '../../../../environments/environment';
import { MatSelect } from '@angular/material/select';
import { take, takeUntil } from 'rxjs/operators';
import {FormControl, Validators} from '@angular/forms';

interface Material {
  MaterialCode: Int32Array;
  MaterialDescription: string;
}

@Component({
  selector: 'ngx-product-mapping',
  templateUrl: './product-mapping.component.html',
  styleUrls: ['./product-mapping.component.scss']
})
export class ProductMappingComponent implements OnDestroy, OnInit {
  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  DataProduct :  Object;
  DTA : any = {};
  isEditMode : boolean = false;
  isInsertMode : boolean = false;
  isListMode : boolean = true;
  productMap: any[] = [];
  
  materialData: any[] = [];
  prcTypeData: any[] = [];
  private index: number = 0;

    /** control for the selected bank */
    public matCtrl: FormControl = new FormControl();
  
    /** control for the MatSelect filter keyword */
    public matFilterCtrl: FormControl = new FormControl();
  
    /** list of banks filtered by search keyword */
    public filteredMats: ReplaySubject<Material[]> = new ReplaySubject<Material[]>(1);

     /** list of banks filtered by search keyword */
    public filteredMatsFinal: ReplaySubject<Material[]> = new ReplaySubject<Material[]>(1);
  
    @ViewChild('singleSelect', { static: true }) singleSelect: MatSelect;
  
    /** Subject that emits when the component has been destroyed. */
    protected _onDestroy = new Subject<void>();

  constructor(private toastrService: NbToastrService, private datePipe: DatePipe, private http: HttpClient) { 
    this.http.get(SERVER_URL+'api/ProductMapping/GetMaterial/').subscribe(
      data => {           
        this.materialData = JSON.parse(data.toString());
        console.info(JSON.stringify(data));
    }, 
      error=> {
        console.error('Failed = '+JSON.stringify(error));
        this.showToast('top-right', 'warning','Error, Please Check your data');
    });   
     
    this.http.get(SERVER_URL+'api/ProductMapping/GetPriceType/').subscribe(
      data => {           
        this.prcTypeData = JSON.parse(data.toString());
        console.info(JSON.stringify(data));
    }, 
      error=> {
        console.error('Failed = '+JSON.stringify(error));
        this.showToast('top-right', 'warning','Error, Please Check your data');
    });   

  }

  getDatas() : Observable <any> {
    return this.http.get(SERVER_URL+'api/ProductMapping/GetProductMapping');
  }

  getDataMath() : Observable <any> {
    return this.http.get(SERVER_URL+'api/ProductMapping/GetMaterial/');
  }

  ngOnInit(): void {

    this.dtOptions = {
      destroy: true
    };

    this.getDatas().subscribe(data => {
      this.productMap = JSON.parse(data);
      this.dtTrigger.next();
    }); 

    this.getDataMath().subscribe(data => {
      this.filteredMatsFinal = JSON.parse(data);
      //this.dtTrigger.next();
    }); 

    //load the initial bank list
    //this.filteredMatsFinal.next(this.materialData.slice());

    this.matFilterCtrl.valueChanges
    .pipe(takeUntil(this._onDestroy))
    .subscribe(() => {
      this.filterMats();
    });
  }

  protected filterMats() {
   
    if (!this.materialData) {
      return;
    }
    // get the search keyword
    let search = this.matFilterCtrl.value;
    if (!search) {
      this.filteredMats.next(this.materialData.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks

    this.filteredMats.next(
        this.materialData.filter(materialData => materialData.MaterialDescription.toLowerCase().indexOf(search) > -1)
     );
     console.info(JSON.stringify(this.filteredMats['_events'][0]));
     this.filteredMatsFinal = this.filteredMats['_events'][0];
  }

  protected setInitialValue() {
    this.filteredMats
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelect.compareWith = (a: Material, b: Material) => a && b && a.MaterialDescription === b.MaterialDescription;
      });
  }

  ngAfterViewInit() {
    this.setInitialValue();
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
    this.dtTrigger.unsubscribe();
  }

 
  CancelData() {
    this.isEditMode = false;
    this.isInsertMode = false;
    this.isListMode = true;
   }

  EditProductMap(id){
    this.http.get(SERVER_URL+'api/ProductMapping/EditProductMapping/'+id).subscribe(
      data => {           
        this.DTA = data;
        this.DTA.Id = data['Id'];
        this.DTA.MaterialCode = data['MaterialCode'];
        this.DTA.PriceTypeId = data['PriceTypeId'];
        this.isInsertMode = false;
        this.isEditMode = true;
        this.isListMode = false;
        console.info(JSON.stringify(data));
    }, 
      error=> {
        console.error('Failed = '+JSON.stringify(error));
        this.showToast('top-right', 'warning','Error, Please Check your data');
    });    
  }

  SaveData() {
      let PrdMapData = {
      'MaterialCode' : this.DTA.MaterialCode,
      'PriceTypeId' : this.DTA.PriceTypeId	
      } 
        console.log(JSON.stringify(PrdMapData));
        this.http.post(SERVER_URL+'api/ProductMapping/SaveProductMapping',PrdMapData).subscribe(
        data => {   
          console.log(data);
          if(data){
            this.showToast('top-right', 'success','Your Data Success Saved');
          } else {
            this.showToast('top-right', 'warning','Error, Save Failed Please Check your data');
          }
          this.isEditMode = false;
          this.isInsertMode = false;
          this.isListMode = true;
          this.ngOnInit();
          this.DTA = [];
          $('#example').DataTable().destroy();
          this.dtTrigger.next(); 
         
        },         
          error=> {
            console.error('SERVER_URL = '+JSON.stringify(error));
            this.showToast('top-right', 'warning','Error, Please Check your data');
            this.isEditMode = false;
            this.isInsertMode = false;
            this.isListMode = true;
        });
  }

  isFormValid() {
    let formCompleted = this.DTA['PriceTypeId'] != undefined && 
    this.DTA['MaterialCode'] != undefined;
  return !formCompleted;
  }

  UpdateData(id) {
    let PrdMapData = {
        'Id' : this.DTA.Id,
        'PriceTypeId' : this.DTA.PriceTypeId,	
        'MaterialCode' : this.DTA.MaterialCode
    } 
        console.log(JSON.stringify(PrdMapData));
        this.http.post(SERVER_URL+'api/ProductMapping/UpdateProductMapping/'+id,PrdMapData).subscribe(
        data => {    
          console.log(data);
          if(data){
            this.showToast('top-right', 'success','Your Data Success Updated');
          } else {
            this.showToast('top-right', 'warning','Error, Update Failed Please Check your data');
          }
          this.isEditMode = false;
          this.isInsertMode = false;
          this.isListMode = true;
          this.DTA = [];
          this.ngOnInit();
          $('#example').DataTable().destroy();
          this.dtTrigger.next();
          
        },         
          error=> {
            console.error('SERVER_URL = '+JSON.stringify(error));
            this.showToast('top-right', 'warning','Error, Please Check your data');
            this.isEditMode = false;
            this.isInsertMode = false;
            this.isListMode = true;
        });
  }

  async DeleteConfirm(id) {
    this.http.delete(SERVER_URL+'api/ProductMapping/DeleteProductMapping/'+id).subscribe(
      data => {   
        this.isEditMode = false;
        this.isInsertMode = false;
        this.isListMode = true;
        this.showToast('top-right', 'success','Your Data Success Deleted');
        this.ngOnInit();
        $('#example').DataTable().destroy();
        this.dtTrigger.next();
    }, 
      error=> {
        console.error('Failed = '+JSON.stringify(error));
        this.showToast('top-right', 'warning','Error, Delete Failed Please Check your data');
        this.isEditMode = false;
        this.isInsertMode = false;
        this.isListMode = true;
    });
  }

  showToast(position, status, message) {
    this.index += 1;
    this.toastrService.show(
      status || 'Success',
      message,
      { position, status });
  }
  
  showToastErr(position, status, message) {
    this.index += 1;
    this.toastrService.show(
      status || 'Success',
      message,
      { position, status });
  }


 
   AddData() {
     this.DTA = {};
     this.isInsertMode = true;
     this.isEditMode = false;
     this.isListMode = false;
   }

}

import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';
import { HttpClient } from '@angular/common/http';
import { SERVER_URL } from '../../../../environments/environment';

@Component({
  selector: 'ngx-sample-form',
  templateUrl: './sample-form.component.html',
  styleUrls: ['./sample-form.component.scss']
})

 
export class SampleFormComponent implements OnInit {
  MyItems :  Object;
  myData : any[] =
  [
    {
      name: 'Earl of Lemongrab',
      age: 'Unknown',
      species: 'Lemon Candy',
      occupation: 'Earl, Heir to the Candy Kingdom Throne'
    },
    {
      name: 'Bonnibel Bubblegum',
      age: '19',
      species: 'Gum Person',
      occupation: 'Returned Ruler of the Candy Kingdom'
    },
    {
      name: 'Phoebe',
      age: '16',
      species: 'Flame Person',
      occupation: 'Ruler of the Fire Kingdom'
    },
    {
      name: 'Lumpy Space Princess',
      age: '18',
      species: 'Lumpy Space Person', 
      occupation: 'Babysitter'
    },
  ];

  characters: Observable<any[]>;
  columns: string[];

  constructor(  private http: HttpClient) {
    this.http.get(SERVER_URL+'api/SideMenu/GetApps').subscribe(
      data =>  {                
        this.MyItems  = data;          
        //console.info('GetApps = '+JSON.stringify( data));
      } ,
      error => console.error(JSON.stringify( error))
      );    
   }

  getCharacters(): Observable<any[]>{
    return Observable.of(this.myData).delay(100);
  }

  getColumns(): string[]{
    return ["Name", "Age", "Species", "Occupation"]
  }

  ngOnInit() {
    this.columns = this.getColumns(); 
  //["name", "age", "species", "occupation"]
    this.characters = this.getCharacters();
 

  }

}

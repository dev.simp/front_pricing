import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { FormsComponent } from './forms.component';
import { FormInputsComponent } from './form-inputs/form-inputs.component';
import { FormLayoutsComponent } from './form-layouts/form-layouts.component';
import { DatepickerComponent } from './datepicker/datepicker.component';
import { ButtonsComponent } from './buttons/buttons.component';
import { SampleFormComponent } from './sample-form/sample-form.component';
import { MasterPricingComponent } from './master-pricing/master-pricing.component';
import { TransaksiPricingComponent } from './transaksi-pricing/transaksi-pricing.component';
import { ProductMappingComponent } from './product-mapping/product-mapping.component';
import { ReportPricingComponent } from './report-pricing/report-pricing.component';

const routes: Routes = [
  {
    path: '',
    component: FormsComponent,
    children: [
      {
        path: 'inputs',
        component: FormInputsComponent,
      },
      {
        path: 'layouts',
        component: FormLayoutsComponent,
      },
      {
        path: 'layouts',
        component: FormLayoutsComponent,
      },
      {
        path: 'buttons',
        component: ButtonsComponent,
      },
      {
        path: 'datepicker',
        component: DatepickerComponent,
      },      
      {
        path: 'sampleForm',
        component: SampleFormComponent,
      },
      {
        path: 'masterPricing',
        component: MasterPricingComponent,
      },  
       {
        path: 'transaksiPricing',
        component: TransaksiPricingComponent,
      },  
      {
        path: 'productMapping',
        component: ProductMappingComponent,
      },   
      {
        path: 'reportPricing',
        component: ReportPricingComponent,
      },     
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FormsRoutingModule {
}


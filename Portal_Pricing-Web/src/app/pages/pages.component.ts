import { Component } from '@angular/core';
import { MENU_ITEMS } from './pages-menu';
import { HttpClient } from '@angular/common/http';
import { SERVER_URL } from '../../environments/environment';

@Component({
  selector: 'ngx-pages',
  styleUrls: ['pages.component.scss'],
  template: `
    <ngx-one-column-layout>
      <nb-menu [items]="this.menuItems"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-one-column-layout>
  `,
})
export class PagesComponent {
  menuItems : any = []; 

  constructor(
    private http: HttpClient
  ) {
      this.http.get(SERVER_URL+'api/SideMenu/GetMenuItems').subscribe(
            data =>  {        
              this.menuItems = data;          
            //  console.info('Dataku = '+JSON.stringify( data));
             } ,
            error => console.error(JSON.stringify( error))
      );    
  }

  ngOnInit(): void {
    this.http.get(SERVER_URL+'api/ProductMapping/GetProductMapping').subscribe(
      data =>  {        
        //this.menuItems = data;          
       console.info('Datakusssss = '+JSON.stringify( data));
       } ,
      error => console.error(JSON.stringify( error))
);  
  
  
  }
   
  //enu = MENU_ITEMS;  
}

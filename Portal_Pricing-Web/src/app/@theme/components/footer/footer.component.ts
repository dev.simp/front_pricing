import { Component } from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <span class="created-by">Pricing App | <b><a href="https://agriconnect.indoagri" target="_blank">SIMP Portal</a></b> 2020</span>

  `,
})
export class FooterComponent {
}

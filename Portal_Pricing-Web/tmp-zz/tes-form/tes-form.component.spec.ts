import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TesFormComponent } from './tes-form.component';

describe('TesFormComponent', () => {
  let component: TesFormComponent;
  let fixture: ComponentFixture<TesFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TesFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TesFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

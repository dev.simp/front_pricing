import { Component } from '@angular/core';
//import { MENU_ITEMS } from './pages-menu';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'ngx-pages',
  styleUrls: ['pages.component.scss'],
  template: `
    <ngx-one-column-layout>
      <nb-menu [items]="this.menuItems"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-one-column-layout>
  `,
})
export class PagesComponent {
  menuItems : any = []; 

  constructor(
    private http: HttpClient
  ) {
      this.http.get('http://localhost:64198/api/Menu/ngxGetMenuItems').subscribe(
            data =>  {        
              this.menuItems = data;          
              console.info('Dataku = '+JSON.stringify( data));
             } ,
            error => console.error(JSON.stringify( error))
      );    
  }
   
  //enu = MENU_ITEMS;  
}
﻿using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.IO;
using System.IO.Compression;
using System.Data.SqlClient;
using System;
using System.Diagnostics;
using System.Web.Http.Filters;
using System.Web.Http.Controllers;
using System.Web.Routing;
using System.Security.Claims;
using Microsoft.Owin.Security.OAuth;
using Microsoft.AspNet.Identity;
using Portal.DAL.Models;

 
namespace Portal.API.Helper
{
    public class  ActivityLog : ActionFilterAttribute 
    {
        private PORTALEntities db = new PORTALEntities();
        public async override void OnActionExecuting(HttpActionContext filterContext)
        { 
            Utility Util = new  Utility();           
            try
            {
                string controllerName = filterContext.ControllerContext.ControllerDescriptor.ControllerName;
                string actionName = filterContext.ActionDescriptor.ActionName;
                string method = Convert.ToString(filterContext.Request.Method); 

                var routeData = filterContext.ControllerContext.RouteData;
                string paramValue = routeData.Values.ContainsKey("id") ? "/" + routeData.Values["id"].ToString() : "";

                //string userName = HttpContext.Current.User.Identity.GetUserName();
                string userId = HttpContext.Current.User.Identity.GetUserName() ?? "Anonymous"; //.GetUserId();
                string ipAddress = Util.GetClientIPAddress() ;
                short activityCode = -1;
              
                switch (method)
                  {
                      case "GET":
                          activityCode = 0;
                          break;
                      case "POST":
                          activityCode = 1;
                          break;
                      case "PUT":
                          activityCode = 2;
                          break;
                      case "DELETE":
                          activityCode = 3;
                          break;
                      case "PATCH":
                          activityCode = 4;
                          break;
                      default:
                          activityCode = 5;
                          break;
                } 
               
                try
                {
                    ActivityLogModel LogData = new ActivityLogModel()
                    {
                        ActivityCode = activityCode,
                        ActionName = method + "#" + actionName + paramValue,
                        Date_Time = DateTime.Now,
                        FromModule = controllerName,
                        IPAddress = ipAddress,
                        BrowserType = HttpContext.Current.Request.Browser.Type,
                        UserId = userId
                    };
                    db.ActivityLogs.Add(LogData);
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    Util.Log(e.Message == null ? e.InnerException.Message : e.Message  ); 
                    throw e.GetBaseException(); 
                }

            }
            catch (Exception e)
            { 
                Util.Log(e.Message == null ? e.InnerException.Message : e.Message); 
                throw e.GetBaseException(); 
            }
        }



    }
}
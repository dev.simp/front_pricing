﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using Microsoft.AspNet.Identity;
using System.Web.Security;
using System.Web.Routing;
using Portal_Pricing.API.Libraries;

namespace Portal_Pricing.API.Helper
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        private const string _authorizedToken = "Authorization";
        private const string _userAgent = "User-Agent";

        public override void OnAuthorization(HttpActionContext actionContext)
        {          
            bool isAnonymous =  actionContext.ActionDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any()
                 || actionContext.ControllerContext.ControllerDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any();

            if (isAnonymous) return;

            string TokenValue = "";
            string BearerTokenValue = "";
            bool isTokenExist = true;
           
            try {
                TokenValue = actionContext.Request.Headers.GetValues(_authorizedToken).FirstOrDefault();
            }
            catch (Exception ex)
            {
                isTokenExist = false;
            }

            if (!isTokenExist)
            try
            {
                BearerTokenValue = actionContext.Request.Headers.Authorization.ToString();
                TokenValue = BearerTokenValue.Split(' ')[1].Trim()  ;
            }
            catch (Exception ex)
            {
                isTokenExist = false;
            }
            if (TokenValue == "")
            {
                HandleUnathorized(actionContext);
                return;
            }
            string Key = "51mpInD0a6r15p1T";
            string[] userToken = { };

            try
            {
                string dataFromToken = AESCrypto.DecryptString(TokenValue, Key);
                userToken = dataFromToken.Split('~'); //AESCrypto.DecryptString(TokenValue, Key).Split('~');
            }
            catch (Exception ex)
            {
                HandleUnathorized(actionContext);
                return;
            }

            if (userToken != null)
            {
                var authorizedRoles = "";
                if (TokenValue != "") 
                {
                    string tokenUserAgen = userToken[UserClaimType.UserAgent];
                    string tokenExpiration = userToken[UserClaimType.Expire];
                    string tokenRole = userToken[UserClaimType.Role];
                    string tokenUserName = userToken[UserClaimType.UserName];

                    if (HttpContext.Current.Request.UserAgent != tokenUserAgen)
                    {
                        HandleUnathorized(actionContext);
                        return;
                    }

                    bool isExpired = Convert.ToDateTime(tokenExpiration).CompareTo(DateTime.Now) < 0;
                    if (isExpired)
                    {
                        HandleUnathorized(actionContext);
                        return;
                    }

                    System.Security.Principal.IPrincipal principle = System.Threading.Thread.CurrentPrincipal;
                    bool isInRole = true;

                    if (Roles != null)
                    {
                        var nRoles = Roles.Split(',');
                        isInRole = nRoles.Contains(tokenRole);// from R in nRoles where R.con
                    }

                    if (Roles != "" && !isInRole)
                    {
                        HandleUnathorized(actionContext);
                        return;
                    }                                    
                }              
            }           
        }

        private static void HandleUnathorized(HttpActionContext actionContext)
        {
            actionContext.Response = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.Unauthorized,
                Content = new StringContent("You are unauthorized to access this resource")
            };
        }

        private string decryptToken(string token)
        {
            return token;
        }

    }
}
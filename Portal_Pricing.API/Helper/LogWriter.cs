﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using System.Text;
using System.Net.Http;

namespace Portal_Pricing.API.Helper
{
    public class LogWriter
    {
        public static void Log(string message)
        {
            StreamWriter streamWriter = null;

            try
            {
                string date = DateTime.Now.ToString("yyyyMM");
                string folder = AppDomain.CurrentDomain.BaseDirectory + "log\\" + date + "\\";

                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }

                string filename = "log_" + date + ".txt";
                streamWriter = new StreamWriter(folder + filename, true);
                streamWriter.WriteLine(DateTime.Now.ToString("dd-MM-yyy HH:mm:ss") + ":" + message);
                streamWriter.Flush();
                streamWriter.Close();
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }
        }

        public static void ExceptionLog(HttpRequestMessage Req, Exception Ex)
        {
           // string currFolder = AppDomain.CurrentDomain.BaseDirectory + "logs";
            string date = DateTime.Now.ToString("yyyyMM");
            string folder = AppDomain.CurrentDomain.BaseDirectory + "Log\\" + date + "\\";

            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }

            string IPAddress = GetClientIPAddress();
            try
            {
                StringBuilder sb = new StringBuilder();
                string uriRef = Req.RequestUri.ToString();
                string currUser = HttpContext.Current.User.Identity.GetUserName();
                string errMessage = "";
                //if (Ex.InnerException.InnerException.Message != null) errMessage = Ex.InnerException.InnerException.Message; else
                     if (Ex.InnerException != null) errMessage = Ex.InnerException.Message; 
                        else errMessage = Ex.Message;
                sb.Append("# ====Date: " + DateTime.Now.ToString() + " ====#" + Environment.NewLine + "URL: " + uriRef + Environment.NewLine + 
                    "Error: " + errMessage + Environment.NewLine + "From Host:" + IPAddress +
                    Environment.NewLine + "User: " + currUser + Environment.NewLine );
                File.AppendAllText(System.Web.HttpContext.Current.Server.MapPath("~/Log/" + date + "/Exception-log_" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt"), sb.ToString());
                sb.Clear();
            }
            catch (Exception ex)
            {
                Log("Failed write exception log: " + ex.InnerException.Message != null ? ex.InnerException.Message : ex.Message);
                System.Console.WriteLine(ex.Message);
            }
        }

        public static string GetClientIPAddress()
        {
            string ipAddress = "";
            if (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            {
                ipAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
            }
            else if (HttpContext.Current.Request.UserHostAddress.Length != 0)
            {
                ipAddress = HttpContext.Current.Request.UserHostAddress;
            }
            else if (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString() == string.Empty)
            {
                ipAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }
            else
            {
                ipAddress = HttpContext.Current.Request.UserHostAddress;
            }

            return ipAddress;
        }
    }
}
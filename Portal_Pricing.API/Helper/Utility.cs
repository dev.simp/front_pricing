﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Globalization;
using System.Net.Http;
using Microsoft.AspNet.Identity;
using System.Web.Http;

namespace Portal_Pricing.API.Helper
{
    public static class Utility
    {
        //static readonly string PasswordHash = "Dr0wssp4p!";
        //static readonly string SaltKey = "08eac03b80adc33dc7d8fb#e44b7c7b05d3a2c5&11166bdb43*fcb710b03ba919e7s";
        //static readonly string VIKey = "@1B2c3#D4e5F6g7H8";
        

        //public string GetMD5Data(string str)
        //{
        //    MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
        //    byte[] data = md5.ComputeHash(Encoding.ASCII.GetBytes(str));
        //    return Convert.ToBase64String(data);
        //}

        //public string GetMD5HashData(string data)
        //{
        //    MD5 md5 = MD5.Create();

        //    byte[] hashData = md5.ComputeHash(Encoding.Default.GetBytes(data));
        //    StringBuilder returnValue = new StringBuilder();
        //    for (int i = 0; i < hashData.Length; i++)
        //    {
        //        returnValue.Append(hashData[i].ToString());
        //    }

        //    return returnValue.ToString();
        //}

        //public string Encrypt(string plainText)
        //{
        //    byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

        //    byte[] keyBytes = new Rfc2898DeriveBytes(PasswordHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
        //    var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.Zeros };
        //    var encryptor = symmetricKey.CreateEncryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));

        //    byte[] cipherTextBytes;

        //    using (var memoryStream = new MemoryStream())
        //    {
        //        using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
        //        {
        //            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
        //            cryptoStream.FlushFinalBlock();
        //            cipherTextBytes = memoryStream.ToArray();
        //            cryptoStream.Close();
        //        }
        //        memoryStream.Close();
        //    }
        //    return Convert.ToBase64String(cipherTextBytes);
        //}

        //public string Decrypt(string encryptedText)
        //{
        //    byte[] cipherTextBytes = Convert.FromBase64String(encryptedText);
        //    byte[] keyBytes = new Rfc2898DeriveBytes(PasswordHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
        //    var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.None };

        //    var decryptor = symmetricKey.CreateDecryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));
        //    var memoryStream = new MemoryStream(cipherTextBytes);
        //    var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
        //    byte[] plainTextBytes = new byte[cipherTextBytes.Length];

        //    int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
        //    memoryStream.Close();
        //    cryptoStream.Close();
        //    return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount).TrimEnd("\0".ToCharArray());
        //}

        public static void Log(string logMessage)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(DateTime.Now.ToLongTimeString() + " : " + logMessage + "\r\n");
                //, DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString());
                File.AppendAllText(System.Web.HttpContext.Current.Server.MapPath("~/Logs/SVisit-log_" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt"), sb.ToString());
                sb.Clear();
            }
            catch (Exception e)
            { }
        }

        //public static void WriteLog(string msg, string fileName)
        //{
        //    string currFolder = AppDomain.CurrentDomain.BaseDirectory + "logs";

        //    string IPAddress = GetClientIPAddress();
        //    try
        //    {
        //       StringBuilder sb = new StringBuilder();
        //       sb.Append("#========================= Date: " + DateTime.Now.ToString() + " =========================#" + Environment.NewLine);
        //       File.AppendAllText(System.Web.HttpContext.Current.Server.MapPath("~/Logs/" + fileName + "_" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt"), sb.ToString());
        //       sb.Clear();
        //    }
        //    catch (Exception e)
        //    { }
        //}

        //public static void ExceptionLog(HttpRequestMessage Req, Exception Ex)
        //{
        //    string currFolder = AppDomain.CurrentDomain.BaseDirectory + "logs";

        //    string IPAddress = GetClientIPAddress();
        //    try
        //    {
        //        StringBuilder sb = new StringBuilder();
        //        string uriRef = Req.RequestUri.ToString();
        //        string currUser = HttpContext.Current.User.Identity.GetUserName();
        //        string errMessage = "";
        //        if (Ex.InnerException == null) errMessage = Ex.Message; else errMessage = Ex.InnerException.InnerException.Message;
        //        sb.Append("#========================= Date: " + DateTime.Now.ToString() + " =========================#" + Environment.NewLine + "URL: " + uriRef + Environment.NewLine + "Error: " + errMessage + Environment.NewLine + "From Host:" + IPAddress + Environment.NewLine + "User: " + currUser + Environment.NewLine + Environment.NewLine);
        //        File.AppendAllText(System.Web.HttpContext.Current.Server.MapPath("~/Logs/SVisit-log_" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt"), sb.ToString());
        //        sb.Clear();
        //    }
        //    catch (Exception e)
        //    { }
        //}

        public static int minutesTo12PM()
        {
            DateTime.Now.ToString("dd/MM/yyyy");
            DateTime timeTo = DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy") + " 15:59:00", "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            //DateTime timeTo = DateTime.ParseExact("07/12/2017 15:59:00", "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            DateTime timeFrom = DateTime.Now;

            TimeSpan Minutes = timeTo - timeFrom;
            double numOfMinutes = Minutes.TotalMinutes ;
            return Convert.ToInt32(numOfMinutes) + 20;
        }

        public static string GetClientIPAddress()
        {
            string ipAddress = "";
            if (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            {
                ipAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
            }
            else if (HttpContext.Current.Request.UserHostAddress.Length != 0)
            {
                ipAddress = HttpContext.Current.Request.UserHostAddress;
            }
            else if (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString() == string.Empty)
            {
                ipAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }
            else
            {
                ipAddress = HttpContext.Current.Request.UserHostAddress;
            }

            return  ipAddress;
        }                   
                   
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal_Pricing.API.Helper
{
    public class UserClaimType
    {                 
        public const int UserName = 0;
        public const int UserId = 1;
        public const int Role = 2;
        public const int FullName = 3;
        public const int Expire = 4;
        public const int UserAgent = 5;       
    }
}
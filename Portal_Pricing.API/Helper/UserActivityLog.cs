﻿using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.IO;
using System.IO.Compression;
using System.Data.SqlClient;
using System;
using System.Diagnostics;
using System.Web.Http.Filters;
using System.Web.Http.Controllers;
using System.Web.Routing;
using System.Security.Claims;
using Microsoft.Owin.Security.OAuth;
using Microsoft.AspNet.Identity;
using Portal_Pricing.DAL.Models;
using System.Security.Principal;
using Portal_Pricing.API.Libraries;
using Portal_Pricing.API.Helper;

namespace Portal_Pricing.API.Helper
{
    public class  UserActivityLog : ActionFilterAttribute 
    {
        INDOAGRI_DATAEntities Db = new INDOAGRI_DATAEntities();
        private PORTALEntities db = new PORTALEntities();
        public async override void OnActionExecuting(HttpActionContext filterContext)
        { 
           // Utility Util = new  Utility();           
            try
            {
                string controllerName = filterContext.ControllerContext.ControllerDescriptor.ControllerName;
                string actionName = filterContext.ActionDescriptor.ActionName;
                string method = Convert.ToString(filterContext.Request.Method); 

                var routeData = filterContext.ControllerContext.RouteData;
                string paramValue = routeData.Values.ContainsKey("id") ? "/" + routeData.Values["id"].ToString() : "";

                System.Security.Principal.IPrincipal principle = System.Threading.Thread.CurrentPrincipal;
                
                //GenericIdentity genericIdentity;// = new GenericIdentity();                
                //string userName = HttpContext.Current.User.Identity.GetUserName();
                 //HttpContext.Current.User.Identity.Name;// GetUserName() ?? "Anonymous"; //.GetUserId();
               
                string TokenValue = "";
                try
                {
                    TokenValue = filterContext.Request.Headers.GetValues("Authorization").FirstOrDefault();
                }
                catch (Exception ex)
                {

                }

                string[] userToken = { };
                string Key = "51mpInD0a6r15p1T";
                userToken = AESCrypto.DecryptString(TokenValue, Key).Split('~');
                string userName = userToken[UserClaimType.UserId];

                string ipAddress = Utility.GetClientIPAddress();
                short activityCode = -1;
              
                switch (method)
                  {
                      case "GET":
                          activityCode = 0;
                          break;
                      case "POST":
                          activityCode = 1;
                          break;
                      case "PUT":
                          activityCode = 2;
                          break;
                      case "DELETE":
                          activityCode = 3;
                          break;
                      case "PATCH":
                          activityCode = 4;
                          break;
                      default:
                          activityCode = 5;
                          break;
                } 
               
                try
                {
                    ActivityLog LogData = new ActivityLog()
                    {
                        ActivityCode = activityCode,
                        ActionName = method + "#" + actionName + paramValue,
                        Date_Time = DateTime.Now,
                        FromModule = controllerName,
                        IPAddress = ipAddress,
                        BrowserType = HttpContext.Current.Request.Browser.Type,
                        UserName = userName 
                    };
                    Db.ActivityLogs.Add(LogData);
                    Db.SaveChanges();
                }
                catch (Exception ex)
                {
                    LogWriter.Log(ex.GetBaseException().ToString());
                    throw ex.GetBaseException(); 
                }

            }
            catch (Exception ex)
            {
                LogWriter.Log(ex.GetBaseException().ToString());
                throw ex.GetBaseException(); 
            }
        }



    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Newtonsoft.Json;
using System.Web.Http.Cors;
using System.Dynamic;
using Portal_Pricing.DAL.Models;
using Portal_Pricing.API.Helper;
using System.Configuration; 

namespace Portal_Pricing.API.Controllers
{
    public class SideMenuController : ApiController
    {
        PORTALEntities Db = new PORTALEntities();

        public class Application
        {
            public int AppID { get; set; }
            public string AppName { get; set; }
        }

        [CompressFilter]
        [HttpGet]
        public async Task<IHttpActionResult> GetApps()
        {
            List<Application> AppList = new List<Application>();
           
           // var AppItems[] = new [{"AppID","1"}];
            var app = new Application
            {
                AppID = 1,
                AppName = "ESS"
            };
            var app1 = new Application
            {
                AppID = 2,
                AppName = "Surat"
            };
            var app2 = new Application
            {
                AppID = 3,
                AppName = "PPBR"
            };
            var app3 = new Application
            {
                AppID = 4,
                AppName = "Pricing App"
            };
            var app4 = new Application
            {
                AppID = 5,
                AppName = "Master Portal"
            };
            AppList.Add(app);
            AppList.Add(app1);
            AppList.Add(app2);
            AppList.Add(app3);
            AppList.Add(app4);

            return Ok(AppList);
        }
        
        [CompressFilter]
        [HttpGet]
        public async Task<IHttpActionResult> GetMenuItems()
        {

            string AppID = ConfigurationManager.AppSettings["AppID"];
            try
            {
                List<childItem> mnSubLevel_2 = (from T in Db.MenuItems where T.AppID == AppID && T.MLevel == 2 && T.C_hidden == false select new childItem() { ID = T.ID, ParentID = T.ParentID, icon = T.C_faIcon, label = T.C_label, link = T.C_link, hidden = T.C_hidden }).ToList(); //, items = new List<childItem> { }
                List<childItem> mnSubLevel_1 = (from T in Db.MenuItems where T.AppID == AppID && T.MLevel == 1 && T.C_hidden == false select new childItem() { ID = T.ID, ParentID = T.ParentID, icon = T.C_faIcon, label = T.C_label, link = T.C_link, hidden = T.C_hidden }).ToList();
                List<childItem> mnTopLevel = (from T in Db.MenuItems where T.AppID == AppID && T.MLevel == 0 && T.C_hidden == false select new childItem() { ID = T.ID, ParentID = T.ParentID, icon = T.C_faIcon, label = T.C_label, link = T.C_link, hidden = T.C_hidden }).ToList();

                var result = new { mnTopLevel = mnTopLevel, items = mnSubLevel_1 };

                List<childItem> mainMenu = new List<childItem>();
                List<childItem> subMenu = new List<childItem>();

                foreach (var topLvl in mnTopLevel)
                {
                    foreach (var itemLvl_2A in mnSubLevel_2)
                    {
                        itemLvl_2A.children = new List<childItem>();
                    }

                    var mnSubLevel_1A = (from T in mnSubLevel_1 where T.ParentID == topLvl.ID select T).ToList();
                    foreach (var itemLvl_1A in mnSubLevel_1A)
                    {
                        var mnSubLevel_2A = (from T in mnSubLevel_2 where T.ParentID == itemLvl_1A.ID select T).ToList();

                        if (mnSubLevel_2A.Count > 0)
                        {
                            itemLvl_1A.children = mnSubLevel_2A;
                        }
                        else
                            itemLvl_1A.children = new List<childItem>();
                    }

                    if (mnSubLevel_1A.Count > 0)
                    {
                        topLvl.children = mnSubLevel_1A;
                    }
                    else
                        topLvl.children = new List<childItem>();

                    mainMenu.Add(topLvl);

                }
                string myObject = JsonConvert.SerializeObject(mainMenu, Formatting.Indented);

                myObject = myObject.Replace("\r\n", "");
                myObject = myObject.Replace("\"label\"", "\"title\"");
                myObject = myObject.Replace("\"children\": []", "\"fragment\":\"\"");
                List<Object> obj = JsonConvert.DeserializeObject<List<Object>>(myObject);
                return Ok(obj);

               // return Ok(authToken);
            }
            catch (Exception ex)
            {
                LogWriter.ExceptionLog(Request, ex);
                throw ex;
            }
            return BadRequest();
        }   
    }
}

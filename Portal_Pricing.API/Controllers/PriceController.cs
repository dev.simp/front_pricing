﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Newtonsoft.Json;
using System.Web.Http.Cors;
using System.Dynamic;
using Portal_Pricing.DAL.Models;
using Portal_Pricing.API.Helper;

namespace Portal_Pricing.API.Controllers
{
    public class PriceController : ApiController
    {
    INDOAGRI_DATAEntities Db = new INDOAGRI_DATAEntities();

    [HttpGet]
    public async Task<IHttpActionResult> GetPrice(int id)
    {
        //string data;
        try
        {
            var PriceList = (from u in Db.SALES_MOBILE_PRICE where u.ID == id select u).FirstOrDefault();
            string myObject = JsonConvert.SerializeObject(PriceList);
            myObject = myObject.Replace("\r\n", "");
            SALES_MOBILE_PRICE data = JsonConvert.DeserializeObject<SALES_MOBILE_PRICE>(@myObject);
            return Ok(data);
        }
        catch (Exception ex)
        {
            LogWriter.ExceptionLog(Request, ex);
            throw ex.GetBaseException();

        }
       
    }

    [CompressFilter]
    [HttpGet]
    public async Task<string> GetMaterial()
    {
        var product = (from F in Db.SALES_MOBILE_GROUP_PRODUCT
                       orderby F.MaterialDescription ascending
                       select new { MATERIAL = F.Material, MaterialDescription = F.MaterialDescription }).ToList();

        string products = JsonConvert.SerializeObject(product);
        string data = (products);
        return data;
    }

    [CompressFilter]
    [HttpGet]
    public async Task<string> GetPriceType(string id)
    {
        var pricetype = (from F in Db.SALES_MOBILE_PRICETYPETOMTR where F.MaterialCode == id
                         join L in Db.SALES_MOBILE_PRICETYPE on F.PriceTypeId equals L.Id
                         orderby L.PriceTypeName ascending
                         select new { PRICETYPE = L.Id, PriceTypeName = L.PriceTypeName }).ToList();

        string products = JsonConvert.SerializeObject(pricetype);
        products = products.Replace("\r\n", "");
        string data = (products);
        return data;
    }

    [CompressFilter]
    [HttpGet]
    public async Task<string> GetPriceType()
    {
        var pricetype = (from F in Db.SALES_MOBILE_PRICETYPE
                         orderby F.PriceTypeName ascending
                         select new { PRICETYPE = F.Id, PriceTypeName = F.PriceTypeName }).ToList();

        string products = JsonConvert.SerializeObject(pricetype);
        string data = (products);
        return data;
    }


    [HttpPost]
    public async Task<IHttpActionResult> UpdatePrice(int Id, SALES_MOBILE_PRICE PriceData)
    {
        SALES_MOBILE_PRICE price = PriceData;
        bool StatusCode = false;
        if (!ModelState.IsValid)
        {
            return BadRequest(ModelState);
        }

        if (Id != PriceData.ID)
        {
            return BadRequest();
        }

        Db.Entry(PriceData).State = EntityState.Modified;

        try
        {
            await Db.SaveChangesAsync();
            int isSaved = await Db.SaveChangesAsync();
            if (isSaved > 0)
            {
                StatusCode = true;
            }
            else
            {
                StatusCode = false;
            }
        }
        catch (Exception ex)
        {
            LogWriter.ExceptionLog(Request, ex);
            throw ex.GetBaseException();
        }

        return Ok(new { status = StatusCode });
    }


    [HttpPost]
    public async Task<IHttpActionResult> SavePrice(SALES_MOBILE_PRICE PriceData)
    {
        SALES_MOBILE_PRICE price = PriceData;
        bool StatusCode = false;
        try
        {
            Db.SALES_MOBILE_PRICE.Add(price);
            int isSaved = await Db.SaveChangesAsync();
            if(isSaved > 0){
                StatusCode = true;
            } else {
                StatusCode = false;
            }
        }
        catch (Exception ex)
        {
            StatusCode = false;
            LogWriter.ExceptionLog(Request, ex);
            throw ex.GetBaseException();
        }
        return Ok(new { status = StatusCode });
    }

    [HttpGet]
    public async Task<IHttpActionResult> GetPrices()
    {
        var pricedata = (from F in Db.SALES_MOBILE_PRICE
                         join G in Db.SALES_MOBILE_GROUP_PRODUCT on F.MATERIAL equals G.Material
                         join L in Db.SALES_MOBILE_PRICETYPE on F.PRICETYPE equals L.Id
                                 select new
                                 {
                                     ID = F.ID,
                                     MATERIAL = F.MATERIAL,
                                     PRICETYPE = F.PRICETYPE,
                                     CURRENCY = F.CURRENCY,
                                     PRICE = F.PRICE,
                                     DATE = F.DATE,
                                     MaterialDescription = G.MaterialDescription,
                                     PriceTypeName = L.PriceTypeName
                                 }).OrderByDescending(s => s.DATE).ToList();

        string myObject = JsonConvert.SerializeObject(pricedata);
        myObject = myObject.Replace("\r\n", "");
        return Ok(myObject);
    }

    [HttpGet]
    public async Task<string> GetPriceTypeFilter(string mtr, string prc, DateTime df, DateTime dt)
    {
        try
        {
            var materiallist = (from F in Db.V_SALES_MOBILE_PRICE.Where(e => (mtr.Length > 0 ? e.MATERIAL == mtr : e.MATERIAL != null && e.DATE >= df && e.DATE <= dt || e.PRICETYPEID.ToString() == prc))
                               select new
                               {
                                   ID = F.ID,
                                   MATERIAL = F.MATERIAL,
                                   DESCRIPTION = F.DESCRIPTION,
                                   PRICETYPE = F.PRICETYPE,
                                   CURRENCY = F.CURRENCY,
                                   PRICE = F.PRICE,
                                   DATE = F.DATE,
                                   PRICETYPEID = F.PRICETYPEID
                               }).ToList();
            string myObject = JsonConvert.SerializeObject(materiallist);
            myObject = myObject.Replace("\r\n", "");
            return myObject;

        }
        catch (Exception x)
        {
            LogWriter.ExceptionLog(Request, x);
            throw x.GetBaseException();
        }
    }


    [HttpGet]
    public async Task<string> GetReportPrices()
    {
        var pricedata = (from F in Db.V_SALES_MOBILE_PRICE
                         select new
                         {
                             ID = F.ID,
                             MATERIAL = F.MATERIAL,
                             DESCRIPTION = F.DESCRIPTION,
                             PRICETYPE = F.PRICETYPE,
                             PRICE = F.PRICE,
                             DATE = F.DATE,
                             PRICETYPEID = F.PRICETYPEID
                         }).ToList();

        string myObject = JsonConvert.SerializeObject(pricedata);
        myObject = myObject.Replace("\r\n", "");
        return myObject;
    }

    [HttpDelete]
    public async Task<IHttpActionResult> DeletePrice(int id)
    {
        try
        {
           

            SALES_MOBILE_PRICE pricesdata = await Db.SALES_MOBILE_PRICE.FindAsync(id);
            if (pricesdata == null)
            {
                return NotFound();
            }

            Db.SALES_MOBILE_PRICE.Remove(pricesdata);
            await Db.SaveChangesAsync();
        }
        catch (Exception ex)
        {
            LogWriter.ExceptionLog(Request, ex);
            throw ex.GetBaseException();
        }
        return Ok();
    }


    }
}
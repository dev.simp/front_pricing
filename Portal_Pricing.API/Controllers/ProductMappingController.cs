﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Newtonsoft.Json;
using System.Web.Http.Cors;
using System.Dynamic;
using Portal_Pricing.DAL.Models;
using Portal_Pricing.API.Helper;

namespace Portal_Pricing.API.Controllers
{
    public class ProductMappingController : ApiController
    {
        INDOAGRI_DATAEntities Db = new INDOAGRI_DATAEntities();

        [HttpGet]
        public async Task<string> GetProductMapping()
        {
            var RequestDetailList = (from F in Db.SALES_MOBILE_PRICETYPETOMTR
                                     join G in Db.SALES_MOBILE_GROUP_PRODUCT on F.MaterialCode equals G.Material
                                     join L in Db.SALES_MOBILE_PRICETYPE on F.PriceTypeId equals L.Id
                                     select new
                                     {
                                         Id = F.Id,
                                         MaterialCode = F.MaterialCode,
                                         PriceTypeId = F.PriceTypeId,
                                         MaterialDescription = G.MaterialDescription,
                                         PriceTypeName = L.PriceTypeName
                                     }).ToList();

            string myObject = JsonConvert.SerializeObject(RequestDetailList);
            myObject = myObject.Replace("\r\n", "");
            return myObject;
        }


        [CompressFilter]
        [HttpGet]
        public async Task<string> GetMaterial()
        {
            try
            {
                var product = (from F in Db.SALES_MOBILE_GROUP_PRODUCT
                               orderby F.MaterialDescription ascending
                               select new { MaterialCode = F.Material, MaterialDescription = F.MaterialDescription }).ToList();

                string products = JsonConvert.SerializeObject(product);
                string data = (products);
                return data;
            }
            catch (Exception ex)
            {
                LogWriter.ExceptionLog(Request, ex);
                throw ex.GetBaseException();
            }
       }

        [CompressFilter]
        [HttpGet]
        public async Task<string> GetPriceType()
        {
            var pricetype = (from F in Db.SALES_MOBILE_PRICETYPE
                             orderby F.PriceTypeName ascending
                             select new { PriceTypeId = F.Id, PriceTypeName = F.PriceTypeName }).ToList();

            string products = JsonConvert.SerializeObject(pricetype);
            string data = (products);
            return data;
        }


        [HttpDelete]
        public async Task<IHttpActionResult> DeleteProductMapping(int id)
        {
            try
            {
                SALES_MOBILE_PRICETYPETOMTR prodmappingdata = await Db.SALES_MOBILE_PRICETYPETOMTR.FindAsync(id);
                if (prodmappingdata == null)
                {
                    return NotFound();
                }

                Db.SALES_MOBILE_PRICETYPETOMTR.Remove(prodmappingdata);
                await Db.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                LogWriter.ExceptionLog(Request, ex);
                throw ex.GetBaseException();
            }
            return Ok();
        }
        
        [HttpGet]
        public async Task<IHttpActionResult> EditProductMapping(int id)
        {
            //string data;
            try
            {
                var PrdMapList = (from u in Db.SALES_MOBILE_PRICETYPETOMTR where u.Id == id select u).FirstOrDefault();
                string myObject = JsonConvert.SerializeObject(PrdMapList);
                myObject = myObject.Replace("\r\n", "");
                SALES_MOBILE_PRICETYPETOMTR data = JsonConvert.DeserializeObject<SALES_MOBILE_PRICETYPETOMTR>(@myObject);
                return Ok(data);
            }
            catch (Exception ex)
            {
                LogWriter.ExceptionLog(Request, ex);
                throw ex.GetBaseException();

            }

        }

        [HttpPost]
        public async Task<IHttpActionResult> SaveProductMapping(SALES_MOBILE_PRICETYPETOMTR PrdMapData)
        {
            SALES_MOBILE_PRICETYPETOMTR prdmap = PrdMapData;
            bool StatusCode = false;
            try
            {
                Db.SALES_MOBILE_PRICETYPETOMTR.Add(prdmap);
                int isSaved = await Db.SaveChangesAsync();
                if (isSaved > 0)
                {
                    StatusCode = true;
                }
                else
                {
                    StatusCode = false;
                }
            }
            catch (Exception ex)
            {
                StatusCode = false;
                LogWriter.ExceptionLog(Request, ex);
                throw ex.GetBaseException();
            }
            return Ok(new { status = StatusCode });
        }

        [HttpPost]
        public async Task<IHttpActionResult> UpdateProductMapping(int Id, SALES_MOBILE_PRICETYPETOMTR PrdMapData)
        {
            SALES_MOBILE_PRICETYPETOMTR price = PrdMapData;
            bool StatusCode = false;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (Id != PrdMapData.Id)
            {
                return BadRequest();
            }

            Db.Entry(PrdMapData).State = EntityState.Modified;

            try
            {
                await Db.SaveChangesAsync();
                int isSaved = await Db.SaveChangesAsync();
                if (isSaved > 0)
                {
                    StatusCode = true;
                }
                else
                {
                    StatusCode = false;
                }
            }
            catch (Exception ex)
            {
                StatusCode = false;
                LogWriter.ExceptionLog(Request, ex);
                throw ex.GetBaseException();
            }

            return Ok(new { status = StatusCode });
        }
    }
}
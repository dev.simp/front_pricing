﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Portal_Pricing.Auth.Controllers
{
    public class TesController : ApiController
    {
        // GET api/tes
        [Authorize]
        public IEnumerable<string> Get()
        {
            return new string[] { "AA value1", "BB value2" };
        }

        // GET api/tes/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/tes
        public void Post([FromBody]string value)
        {
        }

        // PUT api/tes/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/tes/5
        public void Delete(int id)
        {
        }
    }
}

﻿//using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security.OAuth;
using Portal_Pricing.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace Portal_Pricing.Auth.Providers
{
    public class SimpleAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            PORTALEntities db = new PORTALEntities();
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

            string userName = context.UserName; 
            string password = context.Password;

            //var user = (from U in db.Users where U.UserName == userName select new { U.UserFullName }).FirstOrDefault();
            //string userFull = user.UserFullName;

            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            //identity.AddClaim(new Claim("UserName", userName));
            //identity.AddClaim(new Claim("UserId", userName));
            //identity.AddClaim(new Claim(ClaimTypes.Name,userName));// userFull));
            //identity.AddClaim(new Claim(ClaimTypes.Role, "user"));            
           // if (user == null)
            //{
            //    context.SetError("invalid_grant", "The user name or password is incorrect.");
            //    return;
            //}

            context.Validated(identity);

        }
    }
}
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Portal_Pricing.DAL.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class SALES_MOBILE_STOCK
    {
        public long ID { get; set; }
        public string COMPANY { get; set; }
        public string AREA { get; set; }
        public string PLANT { get; set; }
        public string MATERIAL { get; set; }
        public string COMPANYNAME { get; set; }
        public string PLANTNAME { get; set; }
        public decimal BEGIN_STOCK { get; set; }
        public decimal RECEIVE_STOCK { get; set; }
        public decimal DELV_STOCK { get; set; }
        public decimal IN_TRANSIT { get; set; }
        public decimal OTHER_STOCK { get; set; }
        public decimal ENDING_STOCK { get; set; }
        public string PABRIK { get; set; }
        public decimal OUTSTANDING { get; set; }
        public System.DateTime CREATEDDATE { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal_Pricing.DAL.Models
{
    public class subItem
    {
        public int ID { get; set; }
        public int? ParentID { get; set; }
        public string label { get; set; }
        public string faIcon { get; set; }
        public string link { get; set; }
        public bool? hidden { get; set; }
        public bool? disabled { get; set; }
        public List<subItem> items { get; set; }
    }

    public class childItem
    {
        public int ID { get; set; }
        public int? ParentID { get; set; }
        public string label { get; set; }
        public string icon { get; set; }
        public string link { get; set; }
        public bool? hidden { get; set; }
        public List<childItem> children { get; set; }
    }

}
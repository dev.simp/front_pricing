//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Portal_Pricing.DAL.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class V_SALES_MOBILE_PRICE
    {
        public int ID { get; set; }
        public string MATERIAL { get; set; }
        public string DESCRIPTION { get; set; }
        public Nullable<decimal> PRICE { get; set; }
        public Nullable<System.DateTime> DATE { get; set; }
        public string GROUPPRODUCT { get; set; }
        public string SubGroup { get; set; }
        public string PRICETYPE { get; set; }
        public Nullable<int> PRICETYPEID { get; set; }
        public string CURRENCY { get; set; }
    }
}
